# [javadev.net](https://javadev.net) source codes

<br/>

### Run javadev.net on localhost

    # vi /etc/systemd/system/javadev.net.service

Insert code from javadev.net.service

    # systemctl enable javadev.net.service
    # systemctl start javadev.net.service
    # systemctl status javadev.net.service

http://localhost:4025
